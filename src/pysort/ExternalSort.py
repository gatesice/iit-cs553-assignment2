import os
import utils

def presort(filename, outprefix, begin_line=0, num_lines=0, lines_per_file=1000 * 1000):
    """
    Do presort to the original file, and save each small blocked as a file.
    Each saved file is sorted.
    
    **arguments**:
    
    `filename`: the input file.
    
    `outprefix`: the prefix of output.
                 E.g: with prefix aaa, it will get files with `aaa00000`, `aaa00001` etc.
    
    `begin_line`: from which line does it starts to read.

    `num_lines`: how many lines does it reads.

    `lines_per_file`: for each sorted file, how many lines does each file have.
    
    **return**:
    
    The number of sorted files it created.
    """
    f = open(filename, mode='r')
    fsize = os.path.getsize(filename)
    num_lines = fsize / 100 if num_lines == 0 else num_lines
    if fsize / 100 < begin_line + num_lines:
        num_lines = fsize / 100 - begin_line
    
    lcount, fcount = 0, 0
    f.seek(begin_line * 100)
    
    while lcount < num_lines:
        line_to_read = lines_per_file if num_lines - lcount > lines_per_file else num_lines - lcount
        fout = open('%s.sorted.part-%s' % (outprefix, str(fcount).zfill(5)), mode='w')
        ibuffer = '\r\n'.join(sorted(f.read(line_to_read * 100).split('\r\n')))
        fout.write(ibuffer[2:] + ibuffer[:2])
        fout.close()
        lcount += line_to_read
        fcount += 1

    return fcount

class Merger():
    
    def __init__(self, inprefix, outfile, path_wrapper='.'):
        self.inprefix = inprefix
        self.outfile = outfile
        self.path_wrapper = path_wrapper
        self.writen_size = 0
        
        self.lines_in_buffer = 100 * 1000
        
        self._f_list = utils.get_all_files(self.inprefix, self.path_wrapper)
        
        self._f_paths = dict([(f, os.path.join(path_wrapper, f)) for f in self._f_list])
        self._f_obj = dict([(f, open(self._f_paths[f], mode='r')) for f in self._f_list])
        self._f_buffer = dict([(f, []) for f in self._f_list])
        self._f_line_max = dict([(f, os.path.getsize(self._f_paths[f]) / 100) for f in self._f_list])
        self._f_line_current = dict([(f, 0) for f in self._f_list])
        
        self._of_obj = open(self.outfile, mode='w', buffering=1000*1000)
        return
    
    def remove_file(self, filename):
        # delete file
        if filename not in self._f_list:
            return False
        
        # close and delete the file
        self._f_obj[filename].close()        
        os.remove(self._f_paths[filename])
        
        # remove from dicts
        self._f_list.remove(filename)
        self._f_paths.pop(filename, None)
        if self._f_buffer[filename] == []:
            self._f_buffer.pop(filename, None)
        self._f_line_max.pop(filename, None)
        self._f_line_current.pop(filename, None)
        self._f_obj.pop(filename, None)
        
        return
    
    def next_line(self, filename):
        if len(self._f_buffer[filename]) < 100:
            return None
        s = self._f_buffer[filename][:100]
        self._f_buffer[filename] = self._f_buffer[filename][100:]
        return s
    
    def check_buffer(self, filename, delete_on_necessary=False):
        if filename not in self._f_list:
            if self._f_buffer[filename] == []:
                self._f_buffer.pop(filename, None)
                return
            
        if len(self._f_buffer[filename]) < 100:
            lines_to_read = 100 if self._f_line_max[filename] - self._f_line_current[filename] > 100 else self._f_line_max[filename] - self._f_line_current[filename]
            self._f_buffer[filename] += self._f_obj[filename].read(100 * lines_to_read)
            self._f_line_current[filename] += lines_to_read
            self._of_obj.flush()
            if delete_on_necessary:
                self.check_delete(filename)
        return
    
    def writeline(self, line):
        if len(line) != 100:
            print('[WARNING] writing buffer != 100 bytes!')
        self._of_obj.write(''.join(line))
        self.writen_size += 100
        return
    
    def check_delete(self, filename):
        if self._f_line_current == self._f_line_max:
            self.remove_file(filename)
        return
    
    def pick_minimal(self):
        return min(self._f_buffer.items(), key=lambda x:x[1][:10])[0]
            
    def pick_maximal(self):
        return max(self._f_buffer.items(), key=lambda x:x[1][:10])[0]
    
    def has_file(self):
        return len(self._f_list) > 0
    
    def has_data(self):
        return len(self._f_buffer) > 0
    
    def finish(self):
        self._of_obj.close()
        return
    
    def progress(self):
        return self.writen_size, sum(self._f_line_max.values()) * 100
    
    
def merge(inprefix, outfile, path_wrapper='.'):
    """
    Merge multiple files into a merged file.
    
    **arguments**:
    
    `inprefix`:       the merge() will only merge those files which have a filename starts with this.\n
   
    `outfile`:        the filename of the merged file.\n

    `path_wrapper`:   if the input file doesn't exists in current dir, specify this to that folder.\n
    """
    m = Merger(inprefix, outfile, path_wrapper=path_wrapper)
    progress = 0.0
    while m.has_data():
        f = m.pick_minimal()
        l = m.next_line(f)
        if l != None and len(l) == 100:
            m.writeline(l)
            (x1, x2) = m.progress()
            if float(x1) / x2 >= progress / 100:
                progress += 0.1
                print('Merged %.1f KB / %.1f KB  %.1f%%!' % 
                      ((float(x1) / 1000),
                      (float(x2) / 1000),
                      (float(x1) * 100 / float(x2))))
        m.check_buffer(f, delete_on_necessary=True)
    m.finish()

