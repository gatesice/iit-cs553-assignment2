def test_presort_1():
    """
    This function tests the ExternalSort.presort()
    """
    from ExternalSort import presort
    presort('../../input/1GB.unsorted.txt', '../../output/1GB', 
            begin_line=100, num_lines=333, lines_per_file=100)