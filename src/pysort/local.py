from ExternalSort import (presort,
                          merge)

presort('../input/100MB.unsorted.txt', '../output/100MB', lines_per_file=33*1000)
merge('100MB.sorted.part-', '../output/100MB.sorted.txt', path_wrapper='../output')