def get_all_files(prefix, path_wrapper='.'):
    import os
    ls = os.listdir(os.path.abspath(os.path.join('.', path_wrapper)))
    return filter(lambda x: x.startswith(prefix), ls)