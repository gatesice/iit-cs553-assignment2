type file;

file fmapper <"src/pywordcount/Mapper.py">;
file freducer <"src/pywordcount/Reducer.py">;
file fpywordcount[] <fixed_array_mapper; files="src/pywordcount/MapReduceBase.py, src/pywordcount/Reporter.py">;

app (file cout, file fout) map (file pyfile, file pys[], file datafile, int pid)
{
	python @pyfile "--tmpdir" "tmp" "--pid" pid @datafile stdout=filename(cout);
}

app (file cout, file fout) reduce (file pyfile, file pys[], file outfile)
{
	python @pyfile "tmp" @outfile stdout=filename(cout);
}

int numfiles = toInt(arg("numfiles", "1"));

foreach i in [0:numfiles] {
	file inputfile <single_file_mapper; file=strcat("segments/seg-", i)>;
	file cout <single_file_mapper; file=strcat("console/mapper-",i,".log")>;
	file fout <single_file_mapper; file=strcat("tmp/mapper_",i,".tmp")>;

	(cout, fout) = map (fmapper, fpywordcount, inputfile, i);
}

file cout2 <"console/reducer.log">;
file fout2 <"output/reducer.out">;

(cout2, fout2) = reduce (freducer, fpywordcount, fout2);
