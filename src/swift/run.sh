#!/bin/sh

# $1 should be the inputfile

# suppose the file system:
# ./
# ./src/pywordcount/*.py
# ./src/swift
# ./tmp
# ./segments/
# ./output
# ./input


echo spliting file
split -l 100000 $1 tmp/seg-

i=0
cd tmp
for file in `ls`
do
	echo moving $file to seg-$i
	mv $file ../segments/seg-$i
	i=$(($i+1))
done

echo max file id = $1
#swift src/swift/wordcount.swift -numfiles=$i


