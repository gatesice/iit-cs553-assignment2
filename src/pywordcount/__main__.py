from Mapper import WordCountMapper
from Reducer import DictReducer
from Reporter import ConsoleReporter

import os
import argparse
from threading import Thread
from time import time, strftime

debug = False

## Thread class
class ThreadedMapper(WordCountMapper, Thread):
    def __init__(self, begin, end, filename,
                 name='WordCountMapper',
                 reporter=None,
                 debug=False):
        Thread.__init__(self)
        WordCountMapper.__init__(self, name, reporter, debug)
        self.begin = begin
        self.end = end
        self.filename = filename
        return
    
    def run(self):
        f = open(self.filename, mode='r')
        cursor = self.begin
        f.seek(cursor)
        linecounter = 0
        if cursor > 0: # should ignore the first line if it doesn't at the begin of file
            line = f.readline()
            cursor += len(line)            
            if debug:
                print "(skipped) %s @ %d : %s ||| %s" % (self.name, cursor, line[0:9], line[-10:-1])

        nextprint = 0.00
        while (cursor <= self.end):
            #if debug:
            #    print "Thread %s - CURSOR=%d" % (self.name, cursor)
            line = f.readline()
            if len(line) == 0:
                cursor += 1
                continue
            linecounter += 1
            self.map(linecounter, line)
            cursor += len(line)
            if debug:
                print "%s @ %d : %s ||| %s" % (self.name, cursor, line[0:9], line[-10:-1])
            if float(cursor - self.begin) / (self.end - self.begin) >= nextprint:
                print "%s %s - Progress: %.1f%% Cursor Position: %d" % (strftime('%H:%M:%S'), self.name, 100 * float(cursor - self.begin) / (self.end - self.begin), cursor)
                nextprint += 0.001
            pass
        pass
    pass

## Parse Arguments
parser = argparse.ArgumentParser(description='Do WordCount for specific files!')
parser.add_argument('inputlocation', metavar='INPUTLOCATION', type=str,
                    help='The folder which contains the inputfiles.')
parser.add_argument('outputlocation', metavar='OUTPUTLOCATION', type=str,
                    help='The folder for output files.')
parser.add_argument('--ignore', metavar='FILE', type=str,
                    help='The file that describes which characters to be ignored.')
parser.add_argument('--threads', metavar='NUM', type=int,
                    help='Use how many threads to do the whole program.')
parser.add_argument('--collectall', action='store_true', 
                    help='Collect all output file into one.')
args = parser.parse_args()

print args

## Initialization
try:
    inputfiles = os.listdir(os.path.abspath(args.inputlocation))
except OSError as err:
    print os.path.abspath(args.inputlocation)
    raise err
if not os.path.exists(os.path.abspath(args.outputlocation)):
    os.makedirs(os.path.abspath(args.outputlocation))
reporter = ConsoleReporter()
poll = []
final_reducer = DictReducer(name='FinalReducer')
timetable = {}
ttime = time()

for f in inputfiles:
    # Get the file can be deal with
    # This program don't read subfolder files
    if os.path.isdir(f): continue
    if f.startswith('.'): continue
    ff = os.path.abspath(args.inputlocation + '/' + f)
    print 'Working on %s' % ff
    f_size = os.path.getsize(ff)
    timetable[f] = time()
    poll = []
    
    # start Mappers
    b_size = f_size / args.threads
    b_begin, b_end = 0, b_size - 1
    
    for i in range(0, args.threads):
        if i == args.threads - 1:
            b_end = f_size
        th = ThreadedMapper(begin=b_begin,
                            end=b_end,
                            filename=ff,
                            name='Map_%d' % i,
                            reporter=reporter,
                            debug=debug)
        th.add_skip_patterns(["'", '"', '`', '.', ',', '!', '|', '?', '0',
                              '1', '2', '3', '4', '5', '6', '7', '8', '9',
                              ';', ':', '(', ')', '[', ']', '{', '}'])
        poll.append(th)
        b_begin += b_size
        b_end += b_size
    
    # Start map reduce works.
    for th in poll:
        print 'Starting %s' % th.name
        th.start()
        
    for th in poll:
        th.join()
        
    reducer = DictReducer(name='Reducer')
    for th in poll:
        reducer.reduce(th.output)    
    if args.collectall:
        final_reducer.collect(reducer.output)
    else:
        reducer.writefile(file='%s/%s.out' % (args.outputlocation, f))
    timetable[f] = time() - timetable[f]

if args.collectall:
    final_reducer.writefile(file='%s/work.out' % args.outputlocation)
ttime = time() - ttime

print "number of threads: %d" % args.threads
for t in timetable.items():
    print "file %s used %f seconds." % (t[0], t[1])
print "total use: %f seconds." % ttime
    
