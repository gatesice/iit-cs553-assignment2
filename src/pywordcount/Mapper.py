from MapReduceBase import MapReduceBase

class Mapper():
    
    def map(self, key, value):
        pass
    
    pass


class WordCountMapper(MapReduceBase, Mapper):
    
    def __init__(self, name='WordCountMapper', reporter=None, debug=False):
        MapReduceBase.__init__(self)
        self.skip = []
        self.name = name
        self.debug = debug
        self.reporter = reporter
        
    
    def add_skip_patterns(self, patterns):
        self.skip = self.skip + patterns
    
    def map(self, key, value):
        v_size = len(value)
        
        # Apply ignore characters
        for skip in self.skip:
            value = value.replace(skip, ' ')
        
        value = value.replace('\n', '')
        
        # Split characters
        tokens = value.split(' ')
        for token in tokens:
            if token == '': continue
            if self.debug:
                if token == '': print 'Empty string "" in %s' % self.name
                if token.find('\n') >= 0: print 'String contains newline in %s' % self.name
            self.collect({token:1})
                
        return
    
    pass

if __name__ == '__main__':
    import argparse
    import os
    
    ## Parse Arguments
    parser = argparse.ArgumentParser(description='Do a Mapper task independently.')
    parser.add_argument('inputfile', type=str,
                        help='The inputfile to be mapped.')
    parser.add_argument('--pid', metavar='NUM', type=int,
                        help='The id of current process in the process group.')
    parser.add_argument('--tmpdir', metavar='TMPDIR', type=str,
                        help='The position for intermediate results.')
    args = parser.parse_args()
    
    print args
    
    mapper = WordCountMapper()
    mapper.add_skip_patterns(["'", '"', '`', '.', ',', '!', '|', '?'])
    
    ## Initialization
    filename = args.inputfile
    fullname = os.path.abspath(filename)
    filesize = os.path.getsize(fullname)
    dicrct_output = False
    try:
        tmpfile = os.path.abspath(args.tmpdir) + '/' + ('mapper_%d.tmp' % args.pid)
    except:
        tmpfile = os.path.abspath(args.tmpdir) + '/' + 'mapper.tmp'
        direct_output = True
    if not os.path.exists(os.path.abspath(args.tmpdir)):
        os.makedirs(os.path.abspath(args.tmpdir))
    b_begin = 0
    b_end = filesize
    
    f = open(fullname, mode='r')
    f.seek(b_begin)
    cursor = b_begin
    linenumber = 0
        
    while cursor < b_end:
        line = f.readline()
        if len(line) == 0:
            cursor += 1
            continue
        linenumber += 1
        cursor += len(line)
        mapper.map(linenumber, line)
        
    ## Output the results to file
    if direct_output:
        mapper.writefile(file=tmpfile)
    else:
        mapper.writedict(file=tmpfile)