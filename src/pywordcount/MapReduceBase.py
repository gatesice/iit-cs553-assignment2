class MapReduceBase():
    
    def __init__(self):
        self.output = {}
        return
    
    def collect(self, output):
        assert type(output) == type({})
        for i in output.items():
            if self.output.has_key(i[0]):
                self.output[i[0]] += i[1]
            else:
                self.output[i[0]] = i[1]
        return
            
    def writefile(self, file='output'):
        f = open(file, mode='w')
        tmp = sorted(self.output.items(), key=lambda x: x[1], reverse=True)
        for i in tmp:
            #i[0] = i[0].replace(' ', '')
            #i[0] = i[0].replace('\n', '')
            if i[0] == '' or i[0] == '\n': continue
            f.write('%s\t%d\n' % (i[0], i[1]))
        f.close()
        return
    
    def writedict(self, file='output'):
        f = open(file, mode='w')
        m = __import__('csv')
        w = m.writer(f)
        for key, val in self.output.items():
            w.writerow([key, val])
        f.close()
        return
     
    def readdict(self, file='input'):
        f = open(file, mode='r')
        m = __import__('csv')
        d = {}
        for key, val in m.reader(f):
            self.collect({key:int(val)})
        f.close()
        return
     
    pass