from MapReduceBase import MapReduceBase

class Reducer():
    
    def __init__(self):
        return
    
    def reduce(self, inputvalues, outputvalues):
        pass
    
    pass

class DictReducer(MapReduceBase, Reducer):
    
    def __init__(self, name='DictReducer', reporter=None, debug=False):
        MapReduceBase.__init__(self)
        Reducer.__init__(self)
        self.name = name
        self.debug = debug
        self.reporter = reporter
        return
    
    def reduce(self, inputpairs):
        self.collect(inputpairs)
        return
    
    pass

if __name__ == '__main__':
    import argparse
    import os
    
    ## Parse Arguments
    parser = argparse.ArgumentParser(description='Do a Reduce task independently.')
    parser.add_argument('inputfolder', type=str,
                        help='The folder contains the input files.')
    parser.add_argument('outputfile', type=str,
                        help='The location of the outputfile.')
    
    args = parser.parse_args()
    print args
    
    reducer = DictReducer()
    
    ## Initialization
    try:
        inputfiles = os.listdir(os.path.abspath(args.inputfolder))
    except OSError as err:
        print os.path.abspath(args.inputfolder)
        raise err
    
    if not os.path.exists(os.path.abspath(os.path.join(args.outputfile, '..'))):
        os.makedirs(os.path.abspath(os.path.join(args.outputfile, '..')))
        
    for f in inputfiles:
        ff = os.path.join(args.inputfolder, f)
        reducer.readdict(file=os.path.abspath(ff))
        
    reducer.writefile(file=os.path.abspath(args.outputfile))