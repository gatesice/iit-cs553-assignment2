#!/bin/sh
# This script will init the swift on given machine.
# parameter $1: The number of worker nodes in swift.

# prepare directories we need
cd ~
mkdir -p ~/app
mkdir -p ~/build
mkdir -p ~/work

sudo mkdir /mnt/cs553-data
sudo chown ubuntu:ubuntu /mnt/cs553-data
ln -s /mnt/cs553-data data

# install softwares we need basically
cd ~/build

sudo apt-get update
sudo apt-get -y install default-jdk git build-essential make gfortran openmpi-bin libopenmpi-dev

wget "https://bootstrap.pypa.io/get-pip.py"
sudo python get-pip.py
rm get-pip.py

sudo pip install apache-libcloud

export JAVA_HOME=/usr/lib/jvm/default-java

# download credentials
cd ~/work/
wget "http://apus.2syu.me/fs/cs553_hw2/aws.csv"
wget "http://apus.2syu.me/fs/cs553_hw2/cs553.pem"
chmod 400 cs553.pem

# Download Hadoop package
cd ~/build
wget "http://www.dsgnwrld.com/am/hadoop/common/hadoop-1.2.1/hadoop-1.2.1-bin.tar.gz"
# Extract packate
tar -zxf "hadoop-1.2.1-bin.tar.gz"
rm hadoop-1.2.1-bin.tar.gz
mv hadoop-1.2.1/ ~/app/
# Add Hadoop to the PATH environment variable
export HADOOP_HOME=~/app/hadoop-1.2.1
export PATH=$PATH:$HADOOP_HOME/bin
cd $HADOOP_HOME/conf/
mv core-site.xml core-site.xml.backup.1
mv hdfs-site.xml hdfs-site.xml.backup.1
mv mapred-site.xml mapred-site.xml.backup.1
mv masters masters.backup.1
wget "http://apus.2syu.me/fs/cs553_hw2/hadoop-conf/core-site.xml"
wget "http://apus.2syu.me/fs/cs553_hw2/hadoop-conf/hdfs-site.xml"
wget "http://apus.2syu.me/fs/cs553_hw2/hadoop-conf/mapred-site.xml"
wget "http://apus.2syu.me/fs/cs553_hw2/hadoop-conf/masters"

# Download the swift-0.95 package
wget http://swiftlang.org/packages/swift-0.95-RC6.tar.gz
# Extract package
tar -zxf "swift-0.95-RC6.tar.gz"
rm swift-0.95-RC6.tar.gz
mv swift-0.95-RC6 ~/app/
# Add swift to the PATH environment variable
export SWIFT_HOME=~/app/swift-0.95-RC6
export PATH=$PATH:$SWIFT_HOME/bin

# Configure Swift
# In this part you should have your instance creation limit increased.
cd ~/work
git clone https://github.com/yadudoc/cloud-tutorials.git
cd cloud-tutorials/ec2
mv configs configs.default
wget "http://apus.2syu.me/fs/cs553_hw2/swift/configs"
cd ~/work/cloud-tutorials/ec2


echo export JAVA_HOME=/usr/lib/jvm/default-java >> ~/.bashrc
echo export HADOOP_HOME=~/app/hadoop-1.2.1 >> ~/.bashrc
echo export SWIFT_HOME=~/app/swift-0.95-RC6 >> ~/.bashrc
echo export PATH=$PATH:$HADOOP_HOME/bin >> ~/.bashrc
echo export PATH=$PATH:$SWIFT_HOME/bin >> ~/.bashrc
echo export JAVA_HOME=/usr/lib/jvm/default-java >> ~/app/hadoop-1.2.1/conf/hadoop-env.sh
echo AWS_WORKER_COUNT=$1 >> ~/work/cloud-tutorials/ec2/configs

echo ''
echo 'Script run completed!'
echo 'JAVA_HOME=$JAVA_HOME'
echo 'HADOOP_HOME=$HADOOP_HOME'
echo 'SWIFT_HOME=$SWIFT_HOME'
echo 'PATH=$PATH'

if [ "$1" != "0" ]; then
	echo ''
	echo 'PLEASE login, and run the following command:'
	echo ''
	echo '    source setup.sh'
	echo ''
	echo 'Notice that setup.sh must be run with source command.'
fi
